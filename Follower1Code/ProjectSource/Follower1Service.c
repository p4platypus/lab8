/****************************************************************************
 Module
   Follower1Service.c

 Description
   This is the main service responsible for setting up a PIC32 as a follower
   to a master PIC32 via SPI1
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/Follower1Service.h"

// Hardware
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <sys/attribs.h>                              // for ISR macros

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
void InitSPI1Slave(void);

void __ISR(_SPI_1_VECTOR, IPL7SOFT) SPI1SlaveResponse(void);

/*---------------------------- Module Variables ---------------------------*/
static uint8_t MyPriority;                          // priority variable

static volatile uint8_t InputPinState;              // 7 = high, 5 = low

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitFollower1Service

 Parameters
     uint8_t: the priority of this service

 Returns
     bool: false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition, and does any
     other required initialization for this service
****************************************************************************/
bool InitFollower1Service(uint8_t Priority)
{
    ES_Event_t ThisEvent;

    // Initialize MyPriority with passed in parameter
    MyPriority = Priority;

    // Configure RA4 as output to drive LED
    TRISAbits.TRISA4 = 0;
    // Configure RB4 as input to read pin state
    TRISBbits.TRISB4 = 1;
    // Turn off LED first
    PORTAbits.RA4 = 0;
    // Record initial input pin state
    InputPinState = PORTBbits.RB4 ? 7 : 5;
    
    // Select multi-vector mode for interrupt
    INTCONbits.MVEC = 1;
    // Enable interrupts globally
    __builtin_enable_interrupts();
    
    // Initialize SPI1 into slave mode with a time delay
    uint32_t i;
    for(i = 0; i < 100000; i++) {}
    InitSPI1Slave();
    
    // Signal completion of hardware initialization
    printf("Hardware initialization completed\r\n\r\n");
    
    // post the initial transition event
    ThisEvent.EventType = ES_INIT;
    if(ES_PostToService(MyPriority, ThisEvent) == true)
        return true;
    else
        return false;
}

/****************************************************************************
 Function
     PostFollower1Service

 Parameters
     ES_Event_t: the event to post to the queue

 Returns
     bool: false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this service's queue
****************************************************************************/
bool PostFollower1Service(ES_Event_t ThisEvent)
{
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunFollower1Service

 Parameters
     ES_Event_t: the event to process

 Returns
     ES_Event_t: ES_NO_EVENT if no error ES_ERROR otherwise

 Description
     Records the input pin state and/or turns on/off an LED based on the
     command from the master PIC32
****************************************************************************/
ES_Event_t RunFollower1Service(ES_Event_t ThisEvent)
{
    ES_Event_t ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT;              // assume no errors
    
    switch(ThisEvent.EventType) {
        // If event is LED_ON
        case LED_ON:
            // Print receipt message
            printf("SetPinState HIGH received w/ value = %d\r\n\r\n", ThisEvent.EventParam);
        break;
        
        // If event is LED_OFF
        case LED_OFF:
            // Print receipt message
            printf("SetPinState LOW received w/ value = %d\r\n\r\n", ThisEvent.EventParam);
        break;
        
        // If event is PIN_QUERY
        case PIN_QUERY:
            // Print receipt message
            printf("QueryPinState received\r\n\r\n");
        break;
            
        // Repeat cases as required for relevant events
        default:
            ;
    }
    
    return ReturnEvent;
}

/***************************************************************************
 Private Functions
 ***************************************************************************/
/****************************************************************************
 Function
     InitSPI1Slave
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Initialize SPI1 in slave mode with receive interrupt enabled
****************************************************************************/
void InitSPI1Slave(void) {
    // Disable analog operations on RB14
    ANSELBbits.ANSB14 = 0;
    // Set RB7, RB8, RB14 as inputs  
    TRISBbits.TRISB7 = 1;
    TRISBbits.TRISB8 = 1;
    TRISBbits.TRISB14 = 1;
    // Set RB5 as output
    TRISBbits.TRISB5 = 0;
    
    // Map RB7 to SS1
    SS1R = 0b0100;
    // Map RB8 to SDI1
    SDI1R = 0b0100;
    // Map RB5 to SDO1
    RPB5R = 0b0011;
    
    // Disable SPI1 interrupts locally
    IEC1CLR = _IEC1_SPI1EIE_MASK;
    IEC1CLR = _IEC1_SPI1RXIE_MASK;
    IEC1CLR = _IEC1_SPI1TXIE_MASK;
    // Disable SPI1 operation
    SPI1CONbits.ON = 0;
    
    // Clear SPI1 receive buffer via reading
    uint32_t DummyData = SPI1BUF;
    // Disable enhance buffer mode for SPI1
    SPI1CONbits.ENHBUF = 0;
    
    // Clear SPI1 interrupt flags
    IFS1CLR = _IFS1_SPI1EIF_MASK;
    IFS1CLR = _IFS1_SPI1RXIF_MASK;
    IFS1CLR = _IFS1_SPI1TXIF_MASK;
    // Set SPI1 receive interrupt priority to highest
    IPC7bits.SPI1IP = 7;
    // Set SPI1 receive interrupt sub-priority to highest
    IPC7bits.SPI1IS = 3;
    // Enable SPI1 receive interrupt locally
    IEC1SET = _IEC1_SPI1RXIE_MASK;
    
    // Clear receive overflow flag for SPI1
    SPI1STATbits.SPIROV = 0;        
    // Configure PIC32 into slave mode
    SPI1CONbits.MSTEN = 0;
    // Set CKP = 1
    SPI1CONbits.CKP = 1;
    // Set CKE = 0
    SPI1CONbits.CKE = 0;
    // Set slave select polarity to active low
    SPI1CONbits.FRMPOL = 0;
    // Configure 8-bit transfer for SPI1
    SPI1CONbits.MODE32 = 0;
    SPI1CONbits.MODE16 = 0;

    // Enable SPI1 operation
    SPI1CONbits.ON = 1;
    // Pre-load transmit buffer with 0
    SPI1BUF = 0;
}

/****************************************************************************
 Function
     SPI1SlaveResponse
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Outlines the service routine when SPI1 receive interrupt is triggered
****************************************************************************/
void __ISR(_SPI_1_VECTOR, IPL7SOFT) SPI1SlaveResponse(void) {
    // Read data from receive buffer
    uint8_t SPI1ReceiveBufferData = SPI1BUF;

    // If data is 'e'
    if(SPI1ReceiveBufferData == 'e') {
        // Turn on LED
        PORTAbits.RA4 = 1;
        // Send back 0 via transmit buffer
        SPI1BUF = 0;
        // Generate LED_ON event
        ES_Event_t ThisEvent;
        ThisEvent.EventType = LED_ON;
        ThisEvent.EventParam = SPI1ReceiveBufferData;
        PostFollower1Service(ThisEvent);
    // If data is 'r'
    } else if(SPI1ReceiveBufferData == 'r') {
        // Turn off LED
        PORTAbits.RA4 = 0;
        // Send back 0 via transmit buffer
        SPI1BUF = 0;
        // Generate LED_OFF event
        ES_Event_t ThisEvent;
        ThisEvent.EventType = LED_OFF;
        ThisEvent.EventParam = SPI1ReceiveBufferData;
        PostFollower1Service(ThisEvent);
    // If data is 'l'
    } else if(SPI1ReceiveBufferData == 'l') {
        // Read input pin state
        InputPinState = PORTBbits.RB4 ? 7 : 5;
        // Send back input pin state via transmit buffer
        SPI1BUF = InputPinState;
        // Generate PIN_QUERY event
        ES_Event_t ThisEvent;
        ThisEvent.EventType = PIN_QUERY;
        PostFollower1Service(ThisEvent);
    // Otherwise
    } else {
        // Send back 0 via transmit buffer
        SPI1BUF = 0;
    }
    
    // Clear SPI1 receive interrupt flag
    IFS1CLR = _IFS1_SPI1RXIF_MASK;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
