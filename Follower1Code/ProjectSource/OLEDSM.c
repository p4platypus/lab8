/****************************************************************************
 Module
   OLEDSM.c

 Description
   This is the first service for the OLED scrolling display under the
   Gen2 Events and Services Framework. In this service there is a buffer
   that holds the characters to be written to the display. This service
   implements a state machine that accepts an event that triggers the
   writing and scrolling of the display when the display is not busy and
   respond to a second event that indicates that the display update is
   complete and then transitions back to a not busy state.
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/OLEDSM.h"

// Hardware
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <sys/attribs.h> // for ISR macors

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "ES_DeferRecall.h"

// u8g2
#include "../u8g2Headers/u8g2TestHarness_main.h"
#include "../u8g2Headers/common.h"
#include "../u8g2Headers/spi_master.h"
#include "../u8g2Headers/u8g2.h"
#include "../u8g2Headers/u8x8.h"

/*----------------------------- Module Defines ----------------------------*/
//// these times assume a 10.000mS/tick timing
//#define ONE_SEC 1000
//#define HALF_SEC (ONE_SEC / 2)
//#define TWO_SEC (ONE_SEC * 2)
//#define FIVE_SEC (ONE_SEC * 5)

//#define ENTER_POST     ((MyPriority<<3)|0)
//#define ENTER_RUN      ((MyPriority<<3)|1)
//#define ENTER_TIMEOUT  ((MyPriority<<3)|2)

// Specify maximum display string length for our OLED
#define OLED_BUFFER_LENGTH   14

/*---------------------------- Module Variables ---------------------------*/
// Initialize module level Priority variable
static uint8_t MyPriority;

// Initialize module level State variable
static OLEDState_t CurrentState;

// Add deferral queue for up to 3+1 pending deferral events
static ES_Event_t DeferralQueue[3+1];

// Initialize module level buffer holding characters to be written to display
static uint8_t TextBuffer[OLED_BUFFER_LENGTH];
static const char CalibrationBuffer[OLED_BUFFER_LENGTH+1] = "CALIBRATING...";
static const char SpaceBuffer[OLED_BUFFER_LENGTH+1] = "              ";

// Initialize module level u8g2 variable
extern uint8_t u8x8_pic32_gpio_and_delay(u8x8_t *u8x8, uint8_t msg,
        uint8_t arg_int, void *arg_ptr);
extern uint8_t u8x8_byte_pic32_hw_spi(u8x8_t *u8x8, uint8_t msg,
        uint8_t arg_int, void *arg_ptr);
static u8g2_t u8g2;
static uint16_t offset = 0;     // start at the leftmost character position

// Initialize module level event checking variable
static uint8_t LastPageState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitOLEDSM

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
****************************************************************************/
bool InitOLEDSM(uint8_t Priority)
{
    ES_Event_t ThisEvent;

    // Assign user-defined Priority
    MyPriority = Priority;
  
    // Set CurrentState to initial pseudostate
    CurrentState = InitPState;
    
    // Initialize deferral queue
    ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));

    // Post initial transition event
    ThisEvent.EventType = ES_INIT;
    if (ES_PostToService(MyPriority, ThisEvent) == true)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/****************************************************************************
 Function
     PostOLEDSM

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
****************************************************************************/
bool PostOLEDSM(ES_Event_t ThisEvent)
{
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunOLEDSM

 Parameters
     ES_Event : the event to process

 Returns
     ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
     Accepts an event that triggers the writing and scrolling of the display
     when the display is not busy and respond to a second event that indicates
     that the display update is done and then goes back to a non-busy state
****************************************************************************/
ES_Event_t RunOLEDSM(ES_Event_t ThisEvent)
{
    ES_Event_t ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT;

    switch (CurrentState)
    {
        // If SM is in initial pseudostate
        case InitPState:
        {
            // Only if event is ES_Init
            if(ThisEvent.EventType == ES_INIT) {
                // Set up SPI system for OLED use
                SPI_Init();
                // Build up u8g2 structure with proper values for OLED display
                u8g2_Setup_ssd1306_128x64_noname_f(&u8g2, U8G2_R0,
                        u8x8_byte_pic32_hw_spi, u8x8_pic32_gpio_and_delay);
                // Pass all that stuff onto display to initialize it
                u8g2_InitDisplay(&u8g2);
                // Turn off power save so that display will be on
                u8g2_SetPowerSave(&u8g2, 0);
                // Choose font - mono-spaced with reasonable size
                u8g2_SetFont(&u8g2, u8g2_font_t0_18_mr);
                // Overwrite background color of newly written characters
                u8g2_SetFontMode(&u8g2, 0);
                // Initialize TextBuffer[] with all empty spaces
                uint8_t i;
                for(i = 0; i < OLED_BUFFER_LENGTH; i++)
                    *(TextBuffer + i) = ' ';
                // Pass TextBuffer[] to OLED display for writing and scrolling
                u8g2_FirstPage(&u8g2);
                u8g2_DrawStr(&u8g2, offset, 37, TextBuffer);
                // Initialize LastPageState to what OLED display is first in
                LastPageState = u8g2_NextPage(&u8g2);
                // Set CurrentState to non-busy state
                CurrentState = Waiting;
                // Print status message
                puts("ES_INIT received in OLEDSM\r");
            }
        }
        break;
        
        // When SM is waiting for incoming characters
        case Waiting:
        {
            // If a character is received
            if(ThisEvent.EventType == ES_NEW_KEY) {
                // For each element in TextBuffer[] except for last element,
                // set value of current element to that of next element
                uint8_t i;
                for(i = 0; i < OLED_BUFFER_LENGTH - 1; i++)
                    *(TextBuffer + i) = *(TextBuffer + i + 1);
                // Set value of last element to event parameter of ES_NEW_KEY
                *(TextBuffer + OLED_BUFFER_LENGTH - 1) = ThisEvent.EventParam;
                // Pass TextBuffer[] to OLED display for writing and scrolling
                u8g2_FirstPage(&u8g2);
                u8g2_DrawStr(&u8g2, offset, 37, TextBuffer);
                // Confirm that writing and scrolling is indeed started
                LastPageState = 1;
                // Set CurrentState to busy state
                CurrentState = Writing;
            }
            // If re-calibration button is pressed
            if(ThisEvent.EventType == BUTTON_DOWN) {
                // Pass CalibrationBuffer[] to OLED display for writing
                u8g2_FirstPage(&u8g2);
                u8g2_DrawStr(&u8g2, offset, 37, CalibrationBuffer);
                // Confirm that writing is indeed started
                LastPageState = 1;
                // Set CurrentState to busy state
                CurrentState = Writing;
            }
        }
        break;
        
        // When SM is writing to and scrolling OLED display
        case Writing:
        {
            // If a character is received
            if(ThisEvent.EventType == ES_NEW_KEY) {
                // Put ES_NEW_KEY event into deferral queue 
                ES_DeferEvent(DeferralQueue, ThisEvent);
            }
            // If writing and scrolling are finished
            if(ThisEvent.EventType == ES_NEXT_PAGE) {
                // Set CurrentState back to non-busy state
                CurrentState = Waiting;
                // Recall deferred ES_NEW_KEY events in deferral queue, if any
                ES_RecallEvents(MyPriority, DeferralQueue);
            }  
        }
        break;
        
        // Repeat cases as required for relevant events
        default:
            ;
  }

    return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     OLEDState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
****************************************************************************/
OLEDState_t QueryOLEDSM(void)
{
  return CurrentState;
}

/****************************************************************************
 Function
     Check4NextPage

 Parameters
     None

 Returns
     bool true if OLED display update is complete, false otherwise

 Description
     returns whether OLED display update is complete
****************************************************************************/
bool Check4NextPage(void)
{
    bool ReturnVal = false;
    
    // Only run event checker if OLEDSM is in Writing state
    if(CurrentState == Writing) {
        // Get current OLED display state
        uint8_t CurrentPageState;
        CurrentPageState = u8g2_NextPage(&u8g2);
    
        // Post event ES_NEXT_PAGE if OLED display state changes
        // and if OLED display update is complete
        if((LastPageState != CurrentPageState) && (CurrentPageState == 0)) {
            ES_Event_t ThisEvent;
            ThisEvent.EventType = ES_NEXT_PAGE;
            ThisEvent.EventParam = 1;
            PostOLEDSM(ThisEvent);
            ReturnVal = true;
        }

        // Update last OLED display state
        LastPageState = CurrentPageState;
    }
    
    // Return whether OLED display update is complete
    return ReturnVal;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

