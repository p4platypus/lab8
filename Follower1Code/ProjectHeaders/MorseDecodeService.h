/****************************************************************************
  Header file for MorseElementSM
  based on the Gen2 Events and Services Framework
 ****************************************************************************/

#ifndef MorseDecodeService_H
#define MorseDecodeService_H

// Event Definitions
#include "ES_Events.h"    /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Port.h"      /* defines serial routines */

// Morse character definition for use with Morse code dictionary
typedef struct MorseChar{ 
    char RegChar;
    struct MorseChar* Dot;
    struct MorseChar* Dash;
} MorseChar_t; 

// Public Function Prototypes
bool InitMorseDecodeService(uint8_t Priority);
bool PostMorseDecodeService(ES_Event_t ThisEvent);
ES_Event_t RunMorseDecodeService(ES_Event_t ThisEvent);

#endif /* MorseDecodeService_H */

