/****************************************************************************
  Header file for MorseElementSM
  based on the Gen2 Events and Services Framework
 ****************************************************************************/

#ifndef MorseElementSM_H
#define MorseElementSM_H

// Event Definitions
#include "ES_Events.h"    /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Port.h"      /* defines serial routines */

// State definitions for use with query function
typedef enum {
    InitMorseElements,
    CalWaitForRise,
    CalWaitForFall,
    EOC_WaitRise,
    EOC_WaitFall,
    Decode_WaitRise,
    Decode_WaitFall
} MorseState_t;

// Public Function Prototypes
bool InitMorseElementSM(uint8_t Priority);
bool PostMorseElementSM(ES_Event_t ThisEvent);
ES_Event_t RunMorseElementSM(ES_Event_t ThisEvent);
MorseState_t QueryMorseElementSM(void);
bool CheckMorseEvents(void);
bool CheckButtonEvents(void);

#endif /* MorseElementSM_H */

