#include "../u8g2Headers/spi_master.h"
 //tweaked for PIC32MX170F256B, uses SPI1
         
/****************************************************************************
 Function
    SPI_Init
 Parameters
    void
 Returns
    void
 Description
    set up the SPI system for use. Set the clock phase and polarity, master mode
    clock source, baud rate, SS control, transfer width (8-bits)
 Notes
    don't forget to map the pins & set up the TRIS not only for the SPI pins
    but also for the DC (RB13) & Reset (RB12) pins 
 
****************************************************************************/
void SPI_Init(void){
    
    /*Disable analog operations on RA0, RA1, RB12, RB13, RB14*/
    ANSELAbits.ANSA0 = 0;
    ANSELAbits.ANSA1 = 0;
    ANSELBbits.ANSB12 = 0;
    ANSELBbits.ANSB13 = 0;
    ANSELBbits.ANSB14 = 0;
            
    /*Set RA0, RA1, RB12, RB13, RB14 as outputs*/
    TRISAbits.TRISA0 = 0;
    TRISAbits.TRISA1 = 0;
    TRISBbits.TRISB12 = 0;
    TRISBbits.TRISB13 = 0;
    TRISBbits.TRISB14 = 0;
            
    /*Map RA0 to SS*/
    RPA0R = 0b0011;
            
    /*Map RA1 to SDO*/
    RPA1R = 0b0011;
            
    /*Disable SPI operation*/
    SPI1CONbits.ON = 0;
            
    /*Clear receive buffer via reading*/
    uint32_t SPIreceiveBufferData;
    SPIreceiveBufferData = SPI1BUF;
            
    /*Disable enhance buffer mode*/
    SPI1CONbits.ENHBUF = 0;
            
    /*Calculate and set baud rate*/
    uint32_t Fpb = 20000000;
    uint32_t bitRate = 10000000;
    uint32_t baudRate = Fpb/(2UL*bitRate) - 1UL;
    SPI1BRG = baudRate;
        
    /*Clear the receive overflow flag*/
    SPI1STATbits.SPIROV = 0;
            
    /*Configure PIC32 into master mode*/
    SPI1CONbits.MSTEN = 1;
            
    /*Set CKP = 1*/
    SPI1CONbits.CKP = 1;
            
    /*Set CKE = 0*/
    SPI1CONbits.CKE = 0;
            
    /*Configure 8-bit transfer for SPI*/
    SPI1CONbits.MODE32 = 0;
    SPI1CONbits.MODE16 = 0;
        
    /*Select PBCLK as master clock*/
    SPI1CONbits.MCLKSEL = 0;
            
    /*Enable master mode slave select control*/
    SPI1CONbits.MSSEN = 1;
            
    /*Set slave select polarity to active low*/
    SPI1CONbits.FRMPOL = 0;
            
    /*Enable SPI operation*/
    SPI1CONbits.ON = 1;
}
/****************************************************************************
 Function
    SPI_Tx
 Parameters
    uint8_t data   the 8-bit value to be sent out through the SPI
 Returns
    void
 Description
    write the data to the SPIxBUF and then wait for it to go out (SPITBF)
 Notes
    don't forget to read the buffer after the transfer to prevent over-runs
 
****************************************************************************/
void SPI_Tx(uint8_t data){
    
    /*Write given 8-bit data to transmit buffer when it is not full*/
    while(SPI1STATbits.SPITBF) {}
    SPI1BUF = data;
    
    /*Clear receive buffer via reading*/
    uint8_t SPIreceiveBufferData;
    SPIreceiveBufferData = SPI1BUF;
}

/****************************************************************************
 Function
    SPI_TxBuffer
 Parameters
    uint8_t *buffer, a pointer to the buffer to be transmitted 
    uint8_t length   the number of bytes in the buffer to transmit
 Returns
    void
 Description
    loop through buffer calling SPI_Tx for each element in the buffer
 Notes
 
****************************************************************************/
void SPI_TxBuffer(uint8_t *buffer, uint8_t length){
    
    /*Loop through buffer calling SPI_TX to write each element in buffer
     *to SPIxBUF*/
    uint8_t i;
    for(i = 0; i < length; i++)
        SPI_Tx(*(buffer + i));
}
